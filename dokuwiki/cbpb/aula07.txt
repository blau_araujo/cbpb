[ [[aula06|aula anterior]] | [[::cbpb|índice]] | [[aula08|próxima aula]] ]

====== Curso Básico de Programação em Bash ======

===== Aula 7 - Concatenação de Strings =====

--- //Por [[user:blau|Blau Araujo]] - Feb 01, 2020//

{{tag>bash curso programação shell}}

Na terminologia da programação, **concatenar** é o ato de unir dois ou mais valores do tipo **string** com o propósito de obter um único valor final. Por exemplo, se as strings "**com**", "**porta**" e "**mento**" fossem concatenadas nesta ordem, o resultado seria a string final “**comportamento**”.

Ao longo do nosso curso, nós já fizemos isso diversas vezes, mas está na hora de formalizar esse conhecimento.


===== 7.1 - A substituição de variáveis =====

**Sob certas condições**, seja no terminal ou em scripts, quando fazemos referência a uma variável acrescentando o prefixo "**$**" ao seu nome, o Bash “entende” que o que nós queremos é o valor que essa variável armazena e realiza uma **substituição**.

<note>De um ponto de vista mais técnico, este processo é chamado de **expansão de variável**, e nós teremos algumas aulas sobre isso mais adiante.</note>

Observe os exemplos abaixo:


<code>
:~$ cor="verde"

# Sem aspas...
:~$ echo $cor
verde

# Com aspas duplas...
:~$ echo "$cor"
verde

# Com aspas simples...
:~$ echo '$cor'
$cor

# Com aspas duplas e caractere de escape...
:~$ echo "\$cor"
$cor
</code>

Repare que, nos dois últimos casos, o Bash não substituiu a variável e tratou "**$cor**" literalmente como uma string. Isso acontece porque as aspas (ou a ausência delas) e o caractere de escape ( **\** ) também possuem um significado especial na interpretação de comandos, o que pode ser resumido em algumas “regrinhas” simples.

|**Sem Aspas**|Variáveis são expandidas, mas caracteres invisíveis (quebras de linha, múltiplos espaços, tabulações, etc) são descartados e substituídos por um espaço.|
|:::|No terminal, se o histórico estiver habilitado, o caractere ''!'' (exclamação) no início da string será interpretado como um comando de busca de eventos e executado.|
|:::|É possível escapar ''$'', ''`'', ''\'', aspas duplas e caracteres invisíveis.|
|:::|A exclamação no início da string pode ser escapada, mas o caractere de escape também seria retornado como parte dessa string.|
|**Aspas Duplas**|Variáveis são expandidas e caracteres invisíveis são preservados.|
|:::|No terminal, se o histórico estiver habilitado, o caractere ''!'' (exclamação) no início da string será interpretado como um comando de busca de eventos e executado.|
|:::|É possível escapar ''$'', ''`'', ''\'', aspas duplas e caracteres invisíveis.|
|:::|A exclamação no início da string pode ser escapada, mas o caractere de escape também seria retornado como parte dessa string.|
|**Aspas Simples**|Variáveis não são expandidas, caracteres invisíveis não são preservados, e todos os caracteres especiais, inclusive a exclamação no início da string, são tratados como caracteres literais.|
|:::|As aspas simples não podem ocorrer dentro de aspas simples, pois não haveria como escapá-las.|
|**Escape**|O caractere de escape ( **\** ) faz com que os caracteres com significado especial para o Bash sejam interpretados literalmente, a não ser quando ocorre dentro de aspas simples ou ele mesmo é escapado.|

===== 7.2 - Inserindo strings em strings =====

Uma das formas mais comuns de concatenação, é quando nós utilizamos as variáveis para reservar o espaço (//placeholder//) no local onde uma informação deve ser inserida.

Veja o exemplo:

<code>
:~$ objeto="celular"
:~$ echo "Maria tinha um $objeto."
Maria tinha um celular.

:~$ objeto="lápis"
:~$ echo "Maria tinha um $objeto."
Maria tinha um lápis.

:~$ objeto="carneirinho"
:~$ echo "Maria tinha um $objeto."
Maria tinha um carneirinho.
</code>

Nos três casos, a variável referenciada **$objeto** reservou o espaço onde os valores "celular", "lápis" e "carneirinho" deveriam ser inseridos. 

Veja outro exemplo, agora em um script que nós vamos chamar de ''piada-ruim'':

<code>
#!/usr/bin/env bash

ben=10
leonardo=20
joaosinho=30

# Observe o caractere de escape (\) usado para impedir que a
# quebra de linha seja interpretada pelo shell!

frase="No rateio da conta, o Leonardo dá $leonardo, \
o Ben $ben, e o Joãosinho $joaosinho."

echo $frase
</code>

Quando executado, você veria isso no terminal:

<code>
:~$ ./piada-ruim
No rateio da conta, o Leonardo dá 20, o Ben 10, e o Joãosinho 30.
</code>

===== 7.3 – O operador de concatenação =====

De forma geral, como vimos até aqui, não precisamos de nenhum operador para concatenar strings. Mas, em muitos casos, pode ser necessário anexar valores **ao final** do conteúdo de uma variável. Observe a seguinte situação:

<code>
:~$ fruta="banana"
:~$ cor="verde"
:~$ echo $fruta $cor
banana verde
</code>

Veja que bastou utilizar as duas variáveis separadas por um espaço como parâmetro do comando ''echo'' para que a string “banana verde” fosse exibida no terminal.

Ocorre, porém, o caso em que nós só queremos atualizar o valor armazenado na variável antes de utilizarmos alguma forma de exibi-la. Isso pode ser feito recorrendo ao operador de concatenação ''+=''. Por exemplo...


<code>
:~$ fruta="banana"
:~$ fruta+=" verde"
:~$ echo $fruta
banana verde
:~$ fruta+=" baratinha"
:~$ echo $fruta
banana verde baratinha
</code>

<note> **Importante!** O operador **+=** só serve para fazer concatenações quando estamos trabalhando com strings! Se a variável for (de fato) numérica, ele funcionará como um **operador de incremento**! Por exemplo:</note>

<code>
# String...
:~$ valor=1; valor+=1
:~$ echo $valor
11

# Número inteiro...
:~$ declare -i valor; valor=1; valor+=1
:~$ echo $valor
2
</code>

----

[ [[aula06|aula anterior]] | [[::cbpb|índice]] | [[aula08|próxima aula]] ]

----

Este curso está sendo oferecido gratuitamente e eu ainda pretendo disponibilizar outros, inclusive a continuação avançada do curso de programação em shell/bash. Então, se quiser me dar uma força para continuar fazendo esse trabalho...

[[https://pag.ae/7Vu5Cocjo|{{https://stc.pagseguro.uol.com.br/public/img/botoes/doacoes/209x48-doar-assina.gif}}]]

Desde já, muito obrigado!

----

Comente ou tire dúvidas sobre esta aula no [[https://forum.debxp.org/|nosso fórum]]!

~~NOTOC~~
