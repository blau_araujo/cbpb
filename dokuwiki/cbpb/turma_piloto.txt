[ [[::cbpb|Voltar ao cruso...]] ]

====== Curso Básico de Programação em Bash ======

===== Turma Piloto =====

  * Áleson Medeiros
  * Ivanildo Pereira
  * Ronny Santos
  * Tiago Rocha
  * Cezar
  * Leandro Ramos
  * Raphael Matheus
  * Marcelo Dantas
  * Rui Deleterium
  * Renata Benatti
  * Marcelo Amedi
  * Matiusco
  * Otávio Will
  * Gregório de Lucca

Muito obrigado a todos vocês!

----

Este curso está sendo oferecido gratuitamente e eu ainda pretendo disponibilizar outros, inclusive a continuação avançada do curso de programação em shell/bash. Então, se quiser me dar uma força para continuar fazendo esse trabalho...

[[https://pag.ae/7Vu5Cocjo|{{https://stc.pagseguro.uol.com.br/public/img/botoes/doacoes/209x48-doar-assina.gif}}]]

Desde já, muito obrigado!

----

Comente ou tire dúvidas sobre esta aula no [[https://forum.debxp.org/|nosso fórum]]!

~~NOTOC~~
