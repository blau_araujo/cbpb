[ [[aula07|aula anterior]] | [[::cbpb|índice]] | [[aula09|próxima aula]] ]

====== Curso Básico de Programação em Bash ======

===== Aula 8 - Operações aritméticas =====

--- //Por [[user:blau|Blau Araujo]] - Feb 02, 2020//

{{tag>bash curso programação shell}}

Mais cedo ou mais tarde, qualquer programador irá deparar-se com a necessidade de fazer com que seu programa realize operações matemáticas, nem que sejam as mais simples. Com o Bash não é diferente, e nós temos várias alternativas builtin para lidar com cálculos, **todas limitadas a operações com números inteiros**.

Nesta aula, o nosso foco serão as alternativas builtin e as operações aritméticas mais básicas, mas não podemos deixar de fora algumas soluções para os casos em que o shell sozinho é insuficiente.

<note>Está fora do contexto do nosso curso básico, mas é bom saber que o Bash, quando trata de operações com números, vai muito além dos simples cálculos, possuindo vários recursos para muitas outras operações, como comparações, operações lógicas e cálculos com valores expressos em outras bases de numeração.</note>

===== 8.1 - As operações básicas =====

O Bash é capaz de realizar as operações aritméticas básicas de quatro formas, todas elas limitadas a números inteiros:

  * Comando interno **''let expressão''**
  * Comando composto **''%%(( expressão ))%%''**
  * Expansão aritmética **''%%$(( expressão ))%%''**
  * Declaração **''declare -i variável''**

Todos esses métodos são capazes de lidar com as operações aritméticas básicas, mas funcionam de formas muito diferentes, como veremos a seguir. Antes, porém, nós precisamos conhecer os símbolos que, independente do método, irão representar as operações aritméticas que veremos neste curso.

**Operadores aritméticos**

^  +  |Soma|
^  -  |Subtração|
^  *  |Multiplicação|
^  /  |Divisão|
^  %  |Módulo (resto)|
^  %%**%%  |Potenciação (exponenciação)|

**Operadores de atribuição**

^nome=valor|Atribui um valor a "nome"|
^nome+=valor|Soma um valor ao valor atual em "nome"|
^nome-=valor|Subtrai um valor do valor atual em "nome"|
^nome*=valor|Multiplica o valor atual em "nome" por outro valor|
^nome/=valor|Divide o valor atual em "nome" por outro valor|
^nome%=valor|Substitui o valor atual em "nome" pelo resto da divisão por outro valor|
^nome++|Pós-incremento: retorna o valor atual em "nome" e soma 1|
^nome%%--%%|Pós-decremento: retorna o valor atual em "nome" e subtrai 1|
^++nome|Pré-incremento: atribui a "nome" o seu valor atual mais 1|
^%%--nome%%|Pré-decremento: atribui a "nome" o seu valor atual menos 1|

<note>Como o nome diz, os operadores de atribuição vão **atribuir** o resultado da operação aritmética em questão à variável. Portanto, eles seguem as mesmas regras da criação de variáveis que nós vimos na Aula 4 (Variáveis).</note>

Exemplos:

<code>
# Atribui o valor 10 à variável ‘numero’.
numero=10

# Soma 3 ao valor já definido em ‘numero’.
numero+=3

# Multiplica o valor em ‘numero’ por 2.
numero*=2

# O valor atual em 'numero' seria 26.
</code>

O que seria o mesmo que...

<code>
# Atribui o valor 10 à variável ‘numero’.
numero=10

# Soma 3 ao valor já definido em ‘numero’.
numero=$numero+3

# Multiplica o valor em ‘numero’ por 2.
numero=$numero*2

# E o valor atual em 'numero' seria 26.
</code>

<note>Note que eu estou apenas demonstrando como funcionam **os operadores**, não são comandos que podem ser testados no terminal.</note>

Outro par de operadores muito interessante são os operadores de pós-incremento (''nome++'') e pós-decremento (''nome%%--%%''). Esses operadores fazem com que o Bash execute a adição ou a subtração de 1 apenas **após o processamento do comando**, como no exemplo abaixo:

<code>
:~$ nota=8
:~$ echo $((nota++))
8
:~$ echo $nota
9
</code>

Repare que a saída da operação ''nota++'' foi 8, que era o valor anterior na variável, passando a ser 9 apenas depois de retornar a saída.

O que é diferente do próximo exemplo, onde o valor é alterado no momento do
processamento:

<code>
:~$ nota=8
:~$ echo $((++nota))
9
:~$ echo $nota
9
</code>

Quanto às regras de ordem de precedência (quem é executado primeiro), elas são as mesmas da matemática, onde executa-se primeiro...

  * da esquerda para a direita;
  * o que está entre parêntesis;
  * as exponenciações;
  * as multiplicações e divisões;
  * e, por último, as somas e subtrações.

===== 8.2 – O problema do 'declare -i' =====

Como já vimos, toda variável no Bash é criada como sendo do **tipo indefinido** e seu valor é tratado como uma **string**. Também vimos que, com exceção das arrays associativas, nós não somos obrigados a declarar explicitamente o tipo de valor que será armazenado numa variável.

Porém, quando trabalhamos com valores numéricos, em algumas situações pode ser (ou parecer) interessante explicitar que o valor é um inteiro. Então, nós utilizamos o comando builtin ''declare'':

<code>declare -i nome[=valor] # Onde “=valor” é opcional.</code>

Isso ativa o **atributo de inteiro** da variável, fazendo com que ela seja tratada como um valor numérico e, portanto, capaz de participar de algumas operações aritméticas.

Por exemplo:

<code>
:~$ nota=6
:~$ nota=$nota+2
:~$ echo $nota
6+2
</code>

Porém, ativando o atributo de inteiro da variável ''nota''...

<code>
:~$ declare -i nota=6
:~$ nota=$nota+2
:~$ echo $nota
8
</code>

Nós também poderíamos reatribuir o valor em ''nota'' incrementando seu valor:

<code>
:~$ declare -i nota=6
:~$ nota+=2
:~$ echo $nota
8
</code>

Mas, os problemas começam quando tentamos utilizar outros operadores aritméticos do Bash, por exemplo:

<code>
:~$ declare -i nota=6
:~$ nota-=2
bash: nota-=2: comando não encontrado
</code>

Essa inconsistência já é um sinal de que declarar o valor como inteiro pode não ser uma boa ideia, mesmo que isso permita a realização de operações aritméticas mais básicas.

<note>Ainda que exista uma solução para o uso de outros operadores além do ''+='', esta situação caracteriza um desvio do comportamento esperado desses operadores, o que é um problema sério sob qualquer ponto de vista.</note>

Além disso, a atribuição de incremento só funciona no momento em que estamos fazendo a atribuição em si, como pode ser visto neste exemplo...

<code>
:~$ declare -i nota=6
:~$ nota+=2
:~$ echo $nota
8
:~$ echo $nota+=3
8+=3
</code>

Mas a coisa não para por aí...

Outro problema do uso do ''declare -i'', especialmente nos scripts, é que isso afeta a legibilidade do código e, portanto, dificulta seu entendimento e a correção de erros (bugs). 

Note que, aqui, o problema está na presunção de um comportamento padrão, por exemplo:

<code>
:~$ nota=2+2
:~$ echo $nota
2+2
</code>

Isso é o esperado quando se lê esses comandos, ou seja, o valor em ''nota'' é a **string** ''2+2'', a menos que, em algum lugar ou em algum momento, o valor da variável tenha sido declarado como um inteiro.

<code>
declare -i nota
nota=2+2
echo $nota
4
</code>

Essas inconsistências são motivos muito bons para que o uso do ''declare -i'' seja evitado na realização de operações aritméticas.

===== 8.3 - O comando interno “let” =====

O comando interno ''let'' (no sentido de “tornar”, em inglês), é utilizado para criar variáveis interpretadas como numéricas e realizar operações aritméticas com elas. A rigor, ele apenas interpreta expressões que contenham operandos (nomes de variáveis), valores numéricos inteiros e operadores aritméticos.

Exemplo:

<code>
:~$ let nota=2
:~$ echo $nota
2
:~$ let nota=$nota+2
:~$ echo $nota
4
:~$ let nota-=3
:~$ echo $nota
1
</code>

Você também pode passar várias expressões de uma vez:

<code>
:~$ let nota=2 nota=nota+2 nota-=3
:~$ echo $nota
1
</code>

Repare que, diferente do primeiro exemplo, enquanto não executamos o comando, todas as referências à variável ''nota'' são feitas sem o prefixo ''$''.

Deve ser assim por dois motivos:

  * No contexto do comando, antes dele ser executado, ''nota'' não está na lista de variáveis do shell.

  * E nós nunca utilizamos o ''$'' no termo da esquerda de um comando de atribuição.

<note>**Dica:** pelo menos pela sua sanidade mental, considere que nem sempre é uma boa ideia criar as variáveis no próprio comando let, reservando seu uso prioritariamente para a modificação de valores previamente atribuídos. Prefira sempre fazer algo como no exemplo abaixo:</note>

<code>
:~$ preco=100
:~$ desconto=10
:~$ let total=$preco-$desconto
:~$ echo $total
90
</code>

Neste exemplo, as variáveis ''preco'' e ''desconto'' foram preservadas, e nós utilizamos o
comando ''let'' apenas para gerar uma terceira variável (''total'') com o resultado da
operação.

Por outro lado...

<code>
:~$ desconto=10
:~$ let preco=100 preco-=$desconto
:~$ echo $preco
90
</code>

O valor original de ''preco'' (100) teria que ser atribuído novamente, o que não é nada prático e poderia até ser uma fonte de erros.

Outra característica do comando ''let'', é que ele permite a construção de expressões com espaços entre os termos quando elas estão entre aspas. Por exemplo:

<code>
:~$ let "nota = 5"
:~$ echo $nota
5
</code>

Para interpretar múltiplas expressões com um mesmo comando, nós temos duas alternativas:

**Alternativa 1:** cada expressão que contenha espaços deverá vir entre aspas...

<code>
:~$ let "nota = 5" ++nota
:~$ echo $nota
6
</code>

**Alternativa 2:** tudo entre aspas, mas as expressões devem ser separada por uma vírgula...

<code>
:~$ let "nota = 5, ++nota"
:~$ echo $nota
6
</code>

===== 8.4 – O comando composto '(( expressão ))' =====

Na verdade, o comando composto ''%%(( ... ))%%'' é considerado a forma “padrão” de fazer o Bash interpretar expressões aritméticas. Seu comportamento é quase idêntico ao do comando interno ''let'', mas ele oferece a possibilidade de ser utilizado em estruturas de decisão ''if'' (que também é um comando composto) para avaliar comparações.

Diferente do ''let'', múltiplas expressões devem **sempre** ser separadas por vírgulas:

<code>
:~$ ((nota=2, nota=nota+2, nota-=3))
:~$ echo $nota
1
</code>

E também não precisamos de aspas para usar espaços nas expressões:

<code>
:~$ ((nota = 2, nota = nota + 2, nota -= 3))
:~$ echo $nota
1
</code>

===== 8.4 - A expansão aritmética '$(( expressão ))' =====

As expansões do Bash serão assunto das próximas aulas, mas nós já tivemos uma prévia de algumas delas em vários momentos até aqui, e está na hora de falar de mais uma: a **expansão aritmética**.

Diferente dos comandos ''let'' e ''%%(( ... ))%%'', que apenas manipulam variáveis numéricas, a expansão aritmética efetua as operações e fornece um valor como retorno na saída padrão, o
que torna o trabalho muito mais prático e poderoso quando queremos realizar cálculos com números inteiros.

Veja o exemplo:

<code>
:~$ nota=90
:~$ echo $(($nota+5))
95
</code>

Também é possível utilizar espaços e fazer atribuições no interior da expansão:

<code>
:~$ nota=50
:~$ echo $((nota_joao = $nota + 10))
60
:~$ echo $nota_joao
60
</code>

E ainda interpretar múltiplas expressões separadas por vírgulas:

<code>
:~$ nota=50
:~$ echo $((nota_joao = $nota + 10, nota_maria = $nota + 20))
70
:~$ echo $nota_joao
60
:~$ echo $nota_maria
70
</code>

===== 8.5 - Qual método utilizar? =====

Tirando o método em que nós mudamos o atributo da variável para inteiro (''declare -i''), qualquer uma das demais opções é aceitável.

Pessoalmente, até por uma questão de legibilidade do código, eu prefiro (e recomendo) utilizar a expansão aritmética ''%%$(( ... ))%%'', mas quem manda é sempre a situação.

De qualquer forma, se você souber utilizar bem a expansão, você também saberá utilizar o comando let e o comando composto ''%%(( ... ))%%'' sem dificuldades, e é por isso que nós vamos nos fixar nela ao longo do restante do nosso curso.

===== 8.6 - E os números não inteiros? =====

Infelizmente, o Bash não dá suporte a operações com números não inteiros. Para isso, um dos utilitários mais usados é o ''bc'', que é basicamente uma calculadora programável.

Como nosso curso trata apenas do Bash, eu não vou me alongar no uso do ''bc'', mas acredito que os exemplos abaixo são suficientes para dar uma ideia de como ele funciona.

<code>
# O ‘bc’ lê um arquivo ou uma string da saída padrão
# e efetua a operação. Por padrão, ele retorna a parte
# inteira do resultado...

:~$ bc <<< "7 / 3"
2

# Para ver todo o resultado, nós utilizamos a opção ‘-l’...

:~$ bc -l <<< "7 / 3"
2.33333333333333333333

# Se quisermos um número fixo de casas decimais, nós temos
# que passar o argumento ‘scale’ junto com a operação...

:~$ bc <<< "scale=2; 7 / 3"
2.33
</code>

O ''bc'' não vem instalado por padrão em todas as distribuições, mas não deve ser difícil descobrir como instalá-lo. No Debian e derivados, o comando para instalação é:

<code>
sudo apt install bc
</code>

Você encontra mais informações sobre ele e seus operadores no manual:

<code>
:~$ man bc
</code>


----

[ [[aula07|aula anterior]] | [[::cbpb|índice]] | [[aula09|próxima aula]] ]

----

Este curso está sendo oferecido gratuitamente e eu ainda pretendo disponibilizar outros, inclusive a continuação avançada do curso de programação em shell/bash. Então, se quiser me dar uma força para continuar fazendo esse trabalho...

[[https://pag.ae/7Vu5Cocjo|{{https://stc.pagseguro.uol.com.br/public/img/botoes/doacoes/209x48-doar-assina.gif}}]]

Desde já, muito obrigado!

----

Comente ou tire dúvidas sobre esta aula no [[https://forum.debxp.org/|nosso fórum]]!

~~NOTOC~~
