# Tópico 2 - Antes do primeiro script

## 2.1 - Entendendo a utilidade dos scripts

Vamos relembrar alguns conceitos da primeira aula:

  * O shell é um interpretador que executa comandos que podem vir da entrada padrão (terminal), ou de um arquivo (script).
  * Um script é um arquivo de texto contendo os mesmos comandos e instruções que você digitaria no terminal.
  * Quando trabalhamos com o terminal, estamos usando o shell de forma “interativa”.
  * Quando usamos scripts, estamos usando o shell de forma “não interativa”.

Sendo assim, a utilidade mais básica de um script, é a possibilidade de executar comandos “em lote”, em vez de digitar cada um deles no terminal.

Considere a situação hipotética de um usuário que precise, por algum motivo, executar os quatro comandos abaixo em sequência e com uma certa frequência…

```
:~$ whoami
:~$ hostname
:~$ uptime -p
:~$ uname -rms
```

Esses comandos (que não são comandos internos do Bash) retornariam as seguintes informações:

| Comando | Descrição |
| --- | --- |
| `whoami` | retorna o nome do usuário |
| `hostname` | retorna o nome do host do sistema (nome da máquina) |
| `uptime` | retorna há quanto tempo a máquina está ligada |
| `uname` | retorna informações sobre o kernel do sistema

E a saída no terminal poderia ser algo assim...

```
:~$ whoami
arthur
:~$ hostname
excalibur
:~$ uptime -p
up 2 weeks, 3 days, 19 hours, 42 minutes
:~$ uname -rms
Linux 4.19.0-6-amd64 x86_64
```

Sabendo que o shell permite a execução de comandos a partir de um arquivo, faz muito mais sentido criar um script contendo todos esses comandos e executá-los todos chamando apenas esse script. O conteúdo do arquivo teria a seguinte aparência…

```
#!/usr/bin/env bash

whoami
hostname
uptime -p
uname -rms
```

Observe que, a não ser pela primeira linha do arquivo (da qual falaremos no próximo tópico), que é onde nós indicamos para o shell qual será o interpretador de comandos utilizado, o restante do arquivo contém apenas a mesma lista de comandos que o nosso usuário teria que digitar e executar manualmente no terminal! Ou seja, o nosso usuário hipotético (o `arthur`) poderia salvar esse arquivo com o nome `infos`, ou outro nome qualquer, e executá-lo de uma única vez no terminal.

```
:~$ ./infos
arthur
excalibur
up 2 weeks, 3 days, 19 hours, 42 minutes
Linux 4.19.0-6-amd64 x86_64
```

## 2.2 - Sobre a execução de scripts

Observe que o usuário do exemplo acima chamou seu script utilizando `./` antes do nome do arquivo.

```
:~$ ./infos
```

Isso significa que o arquivo executável do script está na pasta em que ele está trabalhando -- no caso, `/home/arthur`. Se ele estivesse trabalhando em qualquer outro local -- na pasta `~/docs/clientes`, por exemplo -- a forma de executar o script seria a seguinte:

```
:~/docs/clientes$ ~/infos
```

Ou assim:

```
:~/docs/clientes$ /home/arthur/infos
```

Mas, também existe a possibilidade de salvar o arquivo do script em uma pasta específica e chamá-lo a partir de qualquer outra em que você esteja trabalhando sem indicar o caminho completo até ele. Nós fazemos isso configurando uma variável de ambiente chamada `PATH`.

Nós entraremos nos detalhes sobre o $PATH e como configurá-lo em outro tópico. Por enquanto, basta saber que, se o arquivo do nosso usuário hipotético estivesse numa pasta definida na variável de ambiente `PATH`, onde quer que ele estivesse, bastaria executar o script da seguinte forma:

```
:~$ infos
```

O que é bem mais interessante, mas exige alguns cuidados.

## 2.3 - Cuidados e boas práticas

Já que mencionamos "cuidados", vamos aproveitar para falar de alguns deles.

### 2.3.1 - Nunca execute linhas de comando e scripts sem saber o que eles fazem!

Quando estamos aprendendo, é muito comum sairmos digitando (ou até colando) indiscriminadamente qualquer linha de comando ou script que encontramos na internet.

Nunca faça isso, e aqui estão alguns motivos.

  * Deveria ser óbvio, mas comandos nem sempre são inofensivos. Se você não sabe o que um comando faz, a possibilidade de você acabar executando algo capaz de destruir o seu sistema é real!
  * Outra realidade são os scripts maliciosos, feitos com o propósito único de prejudicar os outros.
  * Mesmo que não sejam maliciosos, os scripts e comandos que você executa sem saber para que servem e como funcionam podem não fazer o que você acha que eles fazem.
  * O desenvolvedor do script, ou a pessoa que compartilha um comando imaginando que está ajudando de alguma forma, também pode ser inexperiente, e a solução pode acaber sendo incorreta e até prejudicial.
  * Mesmo que não seja incorreta e o desenvolvedor, ou a pessoa que compartilhou um comando, seja experiente, a solução pode ser simplesmente incompatível com o seu sistema, causando mais problemas do que você já tem.

Portanto, antes de executar qualquer comando ou script no seu sistema, você deve sempre analisar e entender o que ele faz!

> **Importante!** Se você **ainda** não é capaz de analisar comandos ou scripts, busque a orientação de alguém mais experiente que seja da sua confiança, ou simplesmente não execute!

### 2.3.2 - Preste atenção no prompt de comandos!

Como dissemos no primeiro tópico, o símbolo no final do prompt indica se estamos trabalhando como usuário normal (`$`) ou como usuário administrativo (`#`). Como usuário administrativo (ou `root`), você tem privilégios para executar qualquer coisa, e basta um comando errado para danificar de forma irrecuperável o seu sistema!

Observe, porém, que o seu prompt pode estar indicando que você está trabalhando como usuário normal (`$`) mas, ainda assim, você pode estar executando um comando como usuário administrativo! É o que acontece quando precedemos um comando com o comando `sudo`.

> **Nota:** O `sudo` (*substitute user and do* -- "troque de usuário e faça") é um comando (utilitário) que nos dá privilégios temporários de **super usuário**, mas não altera o prompt, o que pode causar alguma confusão. Apesar de pedir uma senha antes de executar o comando que vem depois dele, o que até poderia servir de alerta, o sudo possui um tempo de validade. Ou seja, numa mesma sessão, apenas o primeiro comando executado com `sudo` pedirá a senha do usuário.


### 2.3.3 - Seja organizado!

Quando estamos estudando, testando um código, ou quando estamos com pressa, não é raro sairmos criando scripts em qualquer pasta. Esta, definitivamente, não é uma boa prática!

O melhor é sempre ter uma pasta reservada para seus testes e experimentos. Você pode, por exemplo, criar uma pasta chamada `testes` na sua pasta pessoal...

```
:~$ mkdir ~/testes
```

Dentro dela, você pode criar novas pastas para cada assunto que estiver estudando. Assim você não bagunça o seu sistema, não corre o risco de executar algo que não deve no lugar errado, e ainda mantém um controle das coisas que andou experimentando.


### 2.3.4 - Nomes importam!

Ao criar os arquivos dos seus scripts, verifique se ele não tem o mesmo nome de algum comando ou utilitário existente sistema. Na hora de executar qualquer coisa, o shell dará prioridade para os comandos que aparecerem primeiro na lista de caminhos indicados na variável de ambiente `PATH`. Então, é provável que o seu script nem venha a ser executado se ele possuir um nome que conflite com o de algum aplicativo, a menos que você tenha incluído as suas pastas de execução locais antes das pastas globais no `PATH`, o que é muito errado!

Outro problema da falta de atenção com os nomes dos arquivos, aconteceria no caso de você tentar disponibilizar um script para todos os usuários do sistema. Neste caso, o script deveria estar na pasta `/usr/local/bin` ou `/usr/bin`, que é onde ficam todos os executáveis a que todos os usuários têm acesso. Um erro aqui seria ainda mais grave, já que só é possível copiar arquivos para essas pastas como usuário administrativo, e **isso permitiria sobrescrever sem qualquer aviso um aplicativo ou utilitário do sistema com o mesmo nome do seu script!**

### 2.3.5 - Nunca inclua sua pasta de testes no $PATH!

Ainda sobre os caminhos de execução, nunca inclua no `PATH` a sua pasta de testes e experimentos!

Os motivos são até meio óbvios a essa altura, mas nunca é demais alertar que os scripts na pasta de testes **não são projetos finalizados**, eles podem conter erros e causar problemas. Quando você tiver certeza de que eles podem ser usados em produção, basta movê-los para um local definitivo que esteja no `PATH`.

> **Nota:** Essas boas práticas e cuidados referem-se a detalhes a que devemos estar sempre atentos, principalmente no processo de aprendizado. Existem outros cuidados e regras de boas práticas, especialmente os relacionados ao processo de produção de código, que serão vistos ao longo dos próximos tópicos.

## No próximo tópico...

### Nosso primeiro script!

  * As etapas de criação de um script:
    * Criando o arquivo do script;
    * Editando o arquivo do script;
    * Tornando o arquivo do script executável.
  * O nosso primeiro script!