# Curso Básico de Programação em Bash

*Blau Araujo*

  
| Índice                                                |                                                      |
| ----------------------------------------------------- | ---------------------------------------------------- |
| [1 - Conceitos básicos](aula01.md)                    | [vídeo](https://www.youtube.com/watch?v=ZM--I3NJ2jY) |
| [2 - Antes do primeiro script](aula02.md)             | [vídeo](https://www.youtube.com/watch?v=j3ScglKLvq8) |
| [3 - Nosso primeiro script](aula03.md)                | [vídeo](https://www.youtube.com/watch?v=h8X-3bmrjwE) |
| [4 - Variáveis](aula04.md)                            | [vídeo](https://www.youtube.com/watch?v=5JZG9CfxQMw) |
| [5 - Variáveis especiais do shell](aula05.md)         | [vídeo](https://www.youtube.com/watch?v=CFdh6zkSJ2c) |
| [6 - Variáveis indexadas](aula06.md)                  | [vídeo](https://www.youtube.com/watch?v=8XH6i_T2Whc) |
| [7 - Concatenação de strings](aula07.md)              | [vídeo](https://www.youtube.com/watch?v=dF1LJLaWrms) |
| [8 - Operações aritméticas](aula08.md)                | [vídeo](https://www.youtube.com/watch?v=6qwONGj5DxI) |
| [9 - Expansões do shell](aula09.md)                   | [vídeo](https://www.youtube.com/watch?v=1GBHJx53w6c) |
| [10 - Expansão de parâmetros](aula10.md)              | [vídeo](https://www.youtube.com/watch?v=7S3DdzCAk4s) |
| [11 - O loop 'for'](aula11.md)                        | [vídeo](https://www.youtube.com/watch?v=pbdQkk6dSCs) |
| [12 - Os loops 'while' e 'until'](aula12.md)          | [vídeo](https://www.youtube.com/watch?v=t34CwTGP6aE) |
| [13 - Menus e o loop 'select'](aula13,md)             | [vídeo](https://www.youtube.com/watch?v=t34CwTGP6aE) |
| [14 - Controle de fluxo com 'if' e 'case'](aula14.md) | [vídeo](https://www.youtube.com/watch?v=R_XCcwEvdMc) |
| [15 - Funções](aula15.md)                             | [vídeo](https://www.youtube.com/watch?v=84hbaufKM6M) |

