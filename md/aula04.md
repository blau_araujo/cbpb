# Tópico 4 - Variáveis

## 4.1 - Conceito


Uma variável pode ser entendida basicamente como um `NOME` que identifica e aponta para uma determinada informação armazenada na memória do sistema. Esta informação é o  que nós chamamos de `VALOR`.

No shell, nós podemos:

  * criar variáveis;
  * atribuir ou aletrar os valores da variável;
  * eliminar variáveis;
  * ou até alterar os atributos de variáveis.

## 4.2 - Criando variáveis

Para criar uma variável, nós precisamos definir um `NOME` e atribuir a ele um `VALOR`, o que é feito no shell através do operador de atribuição `=`.

Exemplos:

```
:~$ fruta="banana"
:~$ nota=10
:~$ nomes=("João" "Maria" "José")
:~$ retorno=$(whoami)
:~$ soma=$((30+20))
```

> **Importante!** Não existem espaços antes ou depois do operador de atribuição!

Além dessa forma explícita de criar variáveis, nós também podemos atribuir valores a nomes através de comandos. É o caso, por exemplo, do comando interno `read`, que lê informações vindas da entrada padrão (algo que o usuário digite no terminal, por exemplo) e as atribui a um ou vários nomes. Veja o exemplo:

```
:~$ read -p "Digite seu nome: " nome
Digite seu nome: Renata
:~$ echo $nome
Renata
:~$
```

Aqui, o usuário digitou `Renata` e essa informação foi armazenada na variável `nome`. Posteriormente, usando o comando `echo` o conteúdo da variável `nome` foi exibido no terminal. 

Ainda no exemplo acima, repare que quando tentamos ler o conteúdo da variável `nome`, nós incluimos o símbolo `$` no início (`$nome`). É desta forma que nós podemos acessar o conteúdo de uma variável.

> **Nota:** O processo pelo qual o Bash exibe o conteúdo de uma variável e chamado de **expansão**, e isso será assunto de alguns tópicos futuros.

Para ser mais exato, a sintaxe completa para fazermos referência a uma variável é com o nome entre chaves, assim: `${NOME}`. Porém, quando a variável aponta para um único valor (variável **escalar**) e `NOME` aparece na linha seguido de caracteres inválidos ou do fim da linha, as chaves são dispensáveis e podemos usar a forma `$NOME`.

Exemplo:

```
:~$ fruta=banana
:~$ echo $frutada
:~$
```

Repare que a referência à variável `fruta` aparece seguida do caractere `d`, que é válido na composição de um nome de arquivo. Então, do ponto de vista do shell, nós estamos tentando acessar a variável `frutada`, que não existe.

Para resolver isso, nós utilizamos a sintaxe completa:

```
:~$ fruta=banana
:~$ echo ${fruta}da
:~$ bananada
```

## 4.3 - Nomeando variáveis

Os nomes das variáveis podem conter apenas:

  * caracteres maiúsculos e minúsculos de A a Z sem acentos;
  * números, desde que não sejam o primeiro caractere do nome;
  * o caractere sublinhado (`_`).
  
Todos os outros caracteres são **inválidos**!
  
Abaixo, podemos ver alguns exemplos de nomes válidos:

```
fruta   FRUTA     FRUTA1   Fruta1
_fruta  _FRUTA    FRUTA_1  FRU_TA_1
```

Já os nomes abaixo causariam vários problemas...

```
CAPÍTULO  2nome  nome!       RET*
1VAR      VAR 1  nome-aluno  ação
```

## 4.4 - Tipos de variáveis

Diferente de algumas outras linguagens, as variáveis no Bash **não precisam** ser declaradas antes de poderem ser utilizadas.

Uma das consequências disso, é a possibilidade de fazermos referência a uma variável inexistente no script sem que o Bash retorne algum erro. Nesses casos, o valor da variável será **vazio** (*blank*).

Por exemplo, no script `blank_var.sh` abaixo:

```
#!/usr/bin/env bash

echo "Aba${var}te"
var=ca
echo "Aba${var}te"
```

Executando o código, esta seria a saída:

```
:~$ ./blank_var.sh
Abate
Abacate
```

Como podemos ver, a variável `var` não existia antes de ser acessada no primeiro `echo`, e o Bash expandiu seu valor para **vazio** sem retornar qualquer erro.

### 4.4.1 - Tipos de dados

Também não é **necessário** especificar o tipo de valor que será armazenado numa variável. Na verdade, por padrão, todo valor numa variável será considerado pelo Bash como sendo de **tipo indefinido**.

> **Nota:** Repare que, neste momento, estamos falando de "tipos de dados", como *strings*, números inteiros, números de ponto flutuante, valores booleanos, etc.

Como todos os valores são indefinidos **por padrão**, o Bash irá tratá-los inicialmente como se fossem *strings*, mesmo que eles sejam representados com caracteres numéricos. Por exemplo:

```
:~$ a=123
:~$ b=456
:~$ c=$a+$b
:~$ echo $c
123+456
```

Além do fato de não ser esse o jeito certo de efetuar operações aritméticas no Bash, o que aconteceu no exemplo foi uma simples concatenação de strings, ou seja, a string `123` foi unida ao caractere `+` e à string `456` para formar a nova string `123+456`.

> **Nota:** Adiantando um pouco o assunto de outros tópicos, é interessante destacar que, dentro de uma operação aritmética, os valores numéricos seriam automaticamente identificados como tal, como no exemplo abaixo:

```
:~$ a=123
:~$ b=456
:~$ c=$(($a+$b))
:~$ echo $c
579
```

### 4.4.2 - Sobre aspas

As aspas possuem um significado muito especial no Bash. O tipo (aspas simples ou duplas) e a ausência ou presença delas faz toda diferença em como os dados são processados e exibidos.

É muito comum vermos alguns programadores utilizando aspas para ter uma indicação visual de que o valor é uma string ou deixando sem aspas quando pretendem indicar que o valor será utilizado posteriormente numa operação aritmética. Isso funciona **quase sempre** como uma convenção visual, mas não tem nada a ver com o comportamento das aspas ou da ausência delas no Bash!

Podemos comprovar isso com uma pequena modificação do exemplo anterior:

```
:~$ a="123"
:~$ b="456"
:~$ c=$(($a+$b))
:~$ echo $c
579
```

Repare que, agora, os números aparecem entre aspas, mas isso não interferiu na operação aritmética.

Por outro lado, quando atribuímos mais de uma palavra à variável, é importante que isso seja feito entre aspas duplas ou simples, como no exemplo:

```
:~$ fruta=banana amassada
bash: amassada: comando não encontrado
:~$
```

Sem aspas, o shell ignorou o meu comando `fruta=banana` e tentou executar `amassada` como se fosse um comando, retornando um erro. Nós podemos verificar que a atribuição foi ignorada executando:

```
:~$ fruta=banana amassada
bash: amassada: comando não encontrado
:~$ echo $fruta

:~$
```

Um outro exemplo para comprovar o que ocorreu:

```
:~$ fruta=banana whoami
blau
:~$ echo $fruta

:~$ 
```

Neste caso, como o comando `whoami` existe, ele foi executado sem qualquer mensagem de erro.

Para resolver este problema, basta envolver a string com aspas duplas ou simples:

```
:~$ fruta="banana amassada"
:~$ echo $fruta
banana amassada
:~$
```

> **Nota:** Existem muitas diferenças entre os usos de aspas simples, duplas ou a ausência das aspas, mas nós veremos tudo sobre isso ao longo dos próximos tópicos.

## 4.4.3 - Variáveis vetoriais

Até aqui, todos os nossos exemplos mostraram variáveis **escalares**, que é como chamamos quando um `NOME` aponta para apenas um `VALOR`. Mas nós também podemos criar variáveis **vetoriais** (também chamadas de "vetores", "matrizes" ou, em inglês, *"arrays"*), que é quando um único `NOME` aponta para uma coleção de valores, cada um deles identificado por um `ÍNDICE`. Por exemplo:

```
:~$ alunos=(João Maria Renata)
```

Aqui, os três valores que serão atribuídos à variável `alunos` aparecem entre parêntesis e separados por um espaço -- `João`, `Maria` e `Renata`. Os parêntesis, logo depois do operador de atribuição `=`, são a sintaxe que nós utilizamos para informar ao Bash que a variável irá receber uma coleção de valores, também chamados de **elementos**:

```
NOME=(VALOR1 VALOR2 ...)
```

Internamente e por padrão, o Bash utilizará os espaços, as tabulações e as quebras de linha como separadores para identificar os limites de cada elemento da *array*. Tendo determinado quais são os elementos, cada um deles receberá um índice começando pelo número zero (`0`). Então, o conteúdo da variável `alunos` será:

```
Índice 0 => João
Índice 1 => Maria
Índice 2 => Renata
```

Ou, já utilizando a notação correta para acessar esses valores:

```
:~$ echo ${alunos[0]}
João
:~$ echo ${alunos[1]}
Maria
:~$ echo ${alunos[2]}
Renata
```

Repare que, como não estamos mais lidando com variáveis escalares, nós somos obrigados a utilizar as chaves. Além disso, é preciso informar o índice do elemento que queremos acessar entre colchetes, desta forma: `${NOME[ÍNDICE]}`.

Se quisermos ver todos os elementos da *array*, basta colocar os símbolos `@` (arroba) ou `*` (asterisco) no lugar do índice. Qualquer um dos dois informará ao Bash que queremos todos os elementos existentes na variável:

```
:~$ echo ${alunos[@]}
João Maria Renata
```

> **Nota:** A diferença entre o uso do `@` ou do `*`, bem como o aprofundamento nos detalhes sobre variáveis vetoriais, serão assunto do tópicos específico sobre vetores.

### 4.4.4 - O comando interno 'declare'

Ao contrário do que o nome nos leva a supor, a função do comando `declare` não é declarar variáveis, mas **alterar ou exibir os atributos e valores** da variável. 

**EXIBINDO OS VALORES E ATRIBUTOS DA VARIÁVEL**

Para exibir os atributos e valores de uma variável, nós usamos o comando `declare` com a opção `-p`. Por exemplo:

```
:~$ fruta=banana
:~$ declare -p fruta
declare -- fruta="banana"
```

Neste exemplo, a coluna correspondente aos atributos da variável fruta apareceu com `--`, o que confirma que esta variável é do tipo **indefinida**. O mesmo aconteceria se o valor na variável fosse um número:

```
:~$ nota=10
:~$ declare -p nota
declare -- nota="10"
```

Isso comprova o que dissemos anteriormente sobre os números também serem tratados como strings por padrão. Mas, o que aconteceria se o valor fosse uma *array*?


```
:~$ alunos=(Maria José João)
:~$ declare -p alunos
declare -a alunos=([0]="Maria" [1]="José" [2]="João")
:~$
```

Desta vez, além dos valores, a coluna dos atributos exibiu `-a`, indicando que se trata de um vetor (array).

**DEFINIR OS ATRIBUTOS DA VARIÁVEL**

Apesar de termos diversas opções para definir os atributos de uma variável, nós vamos nos concentrar em apenas três delas por enquanto:

| Opção | Atributo                               |
|-------|----------------------------------------|
| `-a`  | Ativa o atributo de arrray indexada.   |
| `-A`  | Ativa o atributo de array associativa. |
| `-i`  | Ativa o atributo de número inteiro.    |

Como vimos no exemplo anterior, a variável `alunos` recebeu o atributo de array indexada (`-a`) automaticamente ao ser criada. Isso aconteceu devido à forma como o shell interpretou a sintaxe da atribuição -- os valores estavam entre parêntesis.

O mesmo aconteceria de a atribuição fosse feita desta forma:

```
:~$ aluno[0]=João
:~$ aluno[1]=Maria
:~$ aluno[2]=Renata
:~$ declare -p aluno 
declare -a aluno=([0]="João" [1]="Maria" [2]="Renata")
```

Mas existe um outro tipo de vetor no shell, o chamado **vetor associativo**, que é quando nós utilizamos conjuntos de caracteres alfanuméricos (strings) nos índices dos valores.

```
:~$ marca[Fiat]=Uno
:~$ marca[VW]=Fusca
:~$ marca[Ford]=Corcel
:~$ declare -p marca
declare -a marca=([0]="Corcel")
```

Repare que o shell ignorou as strings nos índices e registrou apenas o últimno valor atribuído. Além disso, o atributo da variável `marca` foi reportado como `-a`, ou seja, uma array indexada.

A explicação desse comportamento é que, para que um vetor possa utilizar strings nos índices e ser considerado um **vetor associativo**, é obrigatório que o atributo `-A` esteja ligado antes das atribuiições de valores serem feitas, como no exemplo abaixo:

```
:~$ declare -A marca
:~$ marca[Fiat]=Uno
:~$ marca[VW]=Fusca
:~$ marca[Ford]=Corcel
:~$ declare -p marca
declare -A marca=([VW]="Fusca" [Fiat]="Uno" [Ford]="Corcel" )
```

> **Nota:** os vetores associativos funcionam como os tipos "dicionário" ou "mapa" de outras linguagens, e frequentemente são chamados desta forma no Bash. Mais sobre isso no tópico em que falaremos especificamente sobre vetores.

Voltando ao caso dos valores representados como números, o atributo `-i` faz com que eles sejam tratados como sendo do tipo inteiro. Veja abaixo:

```
:~$ var1=123
:~$ var2=456
:~$ declare -p var1
declare -- var1="123"
:~$ declare -p var2
declare -- var2="456"
```

Sem a definição do atributo `-i` os valores de `var1` e `var2` serão do tipo indefinido e, portanto, tratados como strings. Mas, ativando o atributo de inteiro...

```
:~$ declare -i var1 var2
:~$ var1=123
:~$ var2=456
:~$ declare -p var1
declare -i var1="123"
:~$ declare -p var2
declare -i var2="456"
```

O que nos permite fazer algo como:

```
:~$ declare -i total
:~$ total=$var1+$var2
:~$ echo $total
579
```

> **Nota:** mais adiante, ainda neste tópico, nós veremos outro atributo muito importante que podemos manipular com o comando `declare`.

## 4.5 - Escopo das variáveis

Quando falamos em **escopo**, estamos nos referindo à visibilidade de uma variável, ou seja, em que outras partes do programa ou do sistema ela está disponível para ser acessada. Sendo assim, a coisa mais importante que temos que entender sobre o escopo de variáveis no shell, é que **ele está diretamente ligado aos processos e subprocessos em execução no sistema**.

### 4.5.1 - Uma breve explicação sobre processos no Linux

Para simplificar, pense em processos como **representações no kernel** dos programas em execução. Sendo representações, os processos atribuem diversas informações ao programa que está sendo executado, tais como:

  * a identificação do processo (PID)
  * a identificação do processo pai (PPID)
  * o usuário a quem o processo pertence (user)
  * o grupo ao qual o processo pertence (group)
  * sua prioridade de execução, etc.

Quando você inicia seu sistema, o primeiro processo a ser executado é o `init`, e todos os outros processos serão ramificações deste processo inicial.

Da mesma forma, ao abrir um terminal no seu ambiente gráfico, ele será uma ramificação, um processo filho do processo deste ambiente gráfico (que será, obviamente, o processo pai). O próprio terminal, por sua vez, fará com que uma nova instância do shell seja executada, gerando mais um processo que será uma ramificação do processo do terminal, e assim por diante com tudo mais que você executar no terminal.

> **Nota:** No contexto especifíco de processos do shell, é muito comum nós utilizarmos o termo **sessão**.

### 4.5.2 - De volta ao escopo das variáveis no shell

Agora que já sabemos o que são **processos**, fica mais fácil entender a relação existente entre eles e o escopo das variáveis no shell. Vamos rever o exemplo do script `blank_var.sh`:

```
#!/usr/bin/env bash

echo "Aba${var}te"
var=ca
echo "Aba${var}te"
```

Executando...

```
:~$ ./blank_var.sh
Abate
Abacate
```

Como vimos, no primeiro `echo` a variável `var` ainda não havia sido definida, o que foi feito apenas na linha seguinte: `var=ca`, o que possibilitou que o segundo `echo` retornasse `Abacate`.

Mas, vamos fazer um experimento: antes de executar o script, ainda no terminal, nós vamos fazer a atribuição de um valor à variável `var`:

```
:~$ var="ixa gen"
:~$ ./blank_var.sh
```

Qual será o resultado? Será que o script vai enxergar a variável `var`, definida no terminal?

```
:~$ var="ixa gen"
:~$ ./blank_var.sh
Abate
Abacate
```

Não, e isso acontece porque o shell interativo e o shell que está rodando o nosso script são dois processos diferentes e as variáveis, da forma como foram criadas, são visíveis apenas dentro da sessão em que são definidas.





---


|**Variáveis locais**|Presentes na sessão atual do shell e disponíveis apenas para ele mesmo.|
|**Variáveis de ambiente**|Disponíveis para todos os processos executados no shell.|
|**Variáveis do shell**|As variáveis especiais que o próprio shell define e que podem ser tanto locais quanto de ambiente.|

Dentro de um script, as variáveis ainda podem ser **globais** (quando estão disponíveis para todas as instruções dentro dele) ou **locais** (quando só podem ser acessadas por algum bloco de código específico).

<note>Por padrão, todas as variáveis criadas dentro de um script são, para o próprio script, globais. Para torná-las locais, é preciso utilizar o comando interno **local**. Nós voltaremos a isso quando estivermos falando de **funções**.</note>

===== 4.5 - Variáveis inalteráveis (read-only) =====

Normalmente, nós podemos criar variáveis com um determinado valor e mudá-lo à vontade quando necessário...

<code>
:~$ fruta="banana"
:~$ echo $fruta
banana
:~$ fruta="laranja"
:~$ echo $fruta
laranja
</code>

Porém, existem casos onde precisamos garantir que um determinado valor não seja mudado. Com o comando interno **readonly**, o shell retornará um erro caso ocorra uma tentativa de alterar o valor da variável.

<code>
:~$ fruta="banana"
:~$ readonly fruta
:~$ fruta="laranja"
bash: fruta: a variável permite somente leitura
</code>

===== 4.6 - Destruindo variáveis =====

Quando criamos uma variável, ela entra para uma lista e passa a ser rastreada pelo shell enquanto durar a sessão. Para remover a variável dessa lista, nós utilizamos o comando interno **unset**.

<code>
:~$ fruta="banana"
:~$ echo $fruta
banana
:~$ unset fruta
:~$ echo $fruta 

:~$
</code>

<note>**Variáveis inalteráveis não podem ser destruídas!** A única forma de deletar uma variável read-only é encerrando (ou reiniciando) a sessão do shell.</note>

===== 4.7 - Atribuindo saídas de comandos a variáveis =====

Também é possível atribuir a saída de um comando a uma variável utilizando as chamadas **substituições de comando**, que nós veremos em detalhes mais adiante no curso. Para isso, basta utilizar uma das sintaxes abaixo:

<code>
:~$ usuario=$(whoami)
:~$ echo $usuario
arthur
</code>

Ou então...

<code>
:~$ maquina=`hostname`
:~$ echo $maquina
excalibur
</code>

===== 4.8 - Acessando os valores das variáveis =====

A essa altura, de tanto utilizarmos o comando **echo**, você já deve saber como acessar
o valor de uma variável. Mas vamos “formalizar” esse conhecimento: para ter acesso
ao valor armazenado numa variável, basta acrescentar o caractere “**$**” ao início de seu
nome, como neste exemplo:


<code>
:~$ read -p "Digite seu nome: " nome
Digite seu nome: Renata
:~$ mensagem="Olá, $nome!"
:~$ echo $mensagem
Olá, Renata!
</code>

No caso das variáveis indexadas (**arrays**), porém, o procedimento é um pouco diferente, dependendo do que queremos ler da variável. Observe o exemplo:

<code>
:~$ nomes=("João" "Renata" "José")
:~$ echo $nomes
João
</code>

Dos três nomes dessa **array**, o shell só retornou o primeiro. Isso acontece porque, sem uma sintaxe especial, o shell irá tratar a variável “**nomes**” como se ela fosse uma **variável escalar**, e não uma **array**. Para acessar o conteúdo de uma array, nós utilizamos o seguinte formato:

<code>
${nome_da_variável[índice]}
</code>

Aplicando essa ideia ao exemplo, para obter o valor de índice “1”, nós faríamos...

<code>
:~$ echo ${nomes[1]}
Renata
</code>

Isso porque os índices de uma array são contados a partir de zero.

----

[ [[aula03|aula anterior]] | [[::cbpb|índice]] | [[aula05|próxima aula]] ]

----

Este curso está sendo oferecido gratuitamente e eu ainda pretendo disponibilizar outros, inclusive a continuação avançada do curso de programação em shell/bash. Então, se quiser me dar uma força para continuar fazendo esse trabalho...

[[https://pag.ae/7Vu5Cocjo|{{https://stc.pagseguro.uol.com.br/public/img/botoes/doacoes/209x48-doar-assina.gif}}]]

Desde já, muito obrigado!

----

Comente ou tire dúvidas sobre esta aula no [[https://forum.debxp.org/|nosso fórum]]!

~~NOTOC~~
